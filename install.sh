#!/usr/bin/env bash
# Supported Env: Ubuntu 18.04 based Linux distro

set -e

if ! which tmux; then
    read -N 1 -p "tmux is not installed. Would you like to install tmux? [Y|n]" shouldInstall
    printf "\n\n"
    case "$shouldInstall" in
        [^Yy])
            printf "Installing tmux...\n"
            if [[ "$OSTYPE" == linux* ]]; then
                sudo apt-get update
                sudo apt-get install -y tmux
            elif [[ "$OSTYPE" == darwin* ]]; then
                brew install tmux
            fi
            printf "Finished installing tmux\n"
            ;;
        *)
            printf "Okay, tmux will not be installed. Exiting...\n"
            ;;
    esac
fi

printf "Installing config tmux.conf to $HOME/.tmux.conf\n"
cp tmux.conf "$HOME/.tmux.conf"

if [[ "$OSTYPE" == linux* ]]; then
    printf "%s\n" "Adding linux-specific config..."
    cat tmux.linux.conf >> "$HOME/.tmux.conf"
elif [[ "$OSTYPE" == darwin* ]]; then
    printf "%s\n" "Adding macos-specific config..."
    cat tmux.mac.conf >> "$HOME/.tmux.conf"
fi
printf "Done\n"
